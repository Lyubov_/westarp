import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '',
      name: 'index',
      component: () => import(/* webpackChunkName: "main" */ './views/Main.vue'),
    },
    {
      path: '/drivers',
      name: 'driver-page',
      component: () => import(/* webpackChunkName: "main" */ './views/DriverPage.vue'),
    },
    {
      path: '/autos',
      name: 'autos-page',
      component: () => import(/* webpackChunkName: "main" */ './views/AutosPage.vue'),
    },
    {
      path: '/calculate',
      name: 'calculate-page',
      component: () => import(/* webpackChunkName: "main" */ './views/CalculatePage.vue'),
    },
  ],
});
