import axios from 'axios';

const key = '8f15a1c1-f236-453a-9cd6-04346c95dc15';
export default {
  getCoordinate(city) {
    return axios
      .get(`https://geocode-maps.yandex.ru/1.x?geocode=${city}&apikey=${key}&format=json`)
      .then((response) => {
        const { error, errors } = response.data;
        if (error) {
          if (Object.keys(error).length) {
            return Promise.reject(error);
          }
        }

        if (errors && Object.keys(errors).length) {
          return Promise.reject(errors);
        }
        return response.data;
      });
  },
};
