import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    drivers: [{
      name: 'John',
      auto: [{
        name: 'Mercedes',
        speed: 130,
      }, {
        name: 'Volvo',
        speed: 70,
      }],
    }, {
      name: 'Colin',
      auto: [{
        name: 'Volvo',
        speed: 90,
      }, {
        name: 'Scania',
        speed: 50,
      }],
    }, {
      name: 'Tyler',
      auto: [{
        name: 'Scania',
        speed: 60,
      }, {
        name: 'Mercedes',
        speed: 120,
      }],
    }],
    autos: [{
      name: 'Mercedes',
    }, {
      name: 'Scania',
    }, {
      name: 'Volvo',
    }],
  },
  getters: {
    getDrivers: state => state.drivers,
    getAutos: state => state.autos,
  },
  mutations: {

  },
  actions: {

  },
});
